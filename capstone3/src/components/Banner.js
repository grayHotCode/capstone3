import {Row, Col, Button} from 'react-bootstrap'

export default function Banner(){
	return(
	<Row>
		<Col className = "p-5">
			<h1>Welcome to Jamir Fruit Store</h1>
			<p>Fresh from the farm.</p>
		</Col>
	</Row>

	)
}