import {useState} from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}) {

console.log(productProp)

// object destructuring 
const {name, description, price, _id} = productProp
// Syntax: const {properties} = propname

// array destructuring
// const [count, setCount] = useState(0)
// const [seats, setSeats] = useState(30)

// console.log(useState(0))

// Syntax: const [getter, setter] =useState(initialValue)

// Hook used is useState- to store the state



// }

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className = "btn btn-primary" to = {`/products/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
