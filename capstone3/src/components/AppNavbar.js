// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
//import { useState} from 'react';
import { Fragment, useContext } from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

/*	//State to store the user information stored in login page.
	const [user, setUser] = useState(localStorage.getItem("email"));
	console.log(user)
*/
	
	const { user } = useContext(UserContext);

	return(
	<Navbar className="p-5" bg="light" expand="lg">
	  <Navbar.Brand as={Link} to="/" exact>Jamir</Navbar.Brand>
	  <Navbar.Toggle aria-controls="basic-navbar-nav" />
	  <Navbar.Collapse id="basic-navbar-nav">
	    <Nav className="mr-auto">
	      <Nav.Link as={Link} to="/" exact>Home</Nav.Link>
	      <Nav.Link as={Link} to="/adminpanel" exact>Dashboard</Nav.Link>
	      <Nav.Link as={Link} to="/products" exact>Products</Nav.Link>
	      { 
	      	(user.id !== null) ?
	      	<Nav.Link as={Link} to="/logout" exact>Logout</Nav.Link>
	      	:
	      	<Fragment>
		      <Nav.Link as={Link} to="/registration" exact>Register</Nav.Link>
		      <Nav.Link as={Link} to="/login" exact>Login</Nav.Link>
		    </Fragment>
	      } 
	    </Nav>
	  </Navbar.Collapse>
	</Navbar>

	)
}
