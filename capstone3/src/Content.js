import {Row, Col, Card, Button} from 'react-bootstrap'


const Content =() => {

	return (
		<Row className = "mt-3 mb-3">
		<Col xs = {12} md = {4} >
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Learn from Home</h2>
					</Card.Title>
					<Card.Text>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo aliquid magnam repellendus quia ex perferendis cupiditate nemo, doloremque iure tempore perspiciatis hic nulla quas adipisci ipsum veritatis laboriosam natus nesciunt.
					</Card.Text>
					<Button variant="primary">Enroll Now</Button>
				</Card.Body>
			</Card>
		</Col>
		<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Study Now, Pay Later</h2>
					</Card.Title>
					<Card.Text>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo aliquid magnam repellendus quia ex perferendis cupiditate nemo, doloremque iure tempore perspiciatis hic nulla quas adipisci ipsum veritatis laboriosam natus nesciunt.
					</Card.Text>
					<Button variant="primary">Enroll Now</Button>
				</Card.Body>
			</Card>
		</Col>
		<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Be Part of Our Community</h2>
					</Card.Title>
					<Card.Text>
						Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo aliquid magnam repellendus quia ex perferendis cupiditate nemo, doloremque iure tempore perspiciatis hic nulla quas adipisci ipsum veritatis laboriosam natus nesciunt.
					</Card.Text>
					<Button variant="primary">Enroll Now</Button>
				</Card.Body>
			</Card>
		</Col>
	</Row>

		// <Card>
		// 	<Card.Body>
		// 		<Card.Title>Sample Course</Card.Title>
		// 		<Card.Subtitle>Description</Card.Subtitle>
		// 		<Card.Text>This is sample course offering</Card.Text>
		// 		<Card.Subtitle>Price:</Card.Subtitle>
		// 		<Card.Text>Php 40, 000</Card.Text>
		// 		<Button variant="primary">Enroll Now</Button>
		// 	</Card.Body>
		// </Card>

		)
}

export default Content