
import {Navbar, Nav} from 'react-bootstrap'

const Header =() => {


	return (
		<Navbar bg="light" expand="lg">
			  <Navbar.Brand className="px-5" href="#home">Zuitt</Navbar.Brand>
			  <Navbar.Toggle aria-controls="basic-navbar-nav" />
			  
			  <Navbar.Collapse id="basic-navbar-nav">
			    <Nav className="mr-auto">
			      <Nav.Link href="#home">Home</Nav.Link>
			      <Nav.Link href="#courses">Courses</Nav.Link>
			    </Nav>
			  </Navbar.Collapse>
		</Navbar>

		)
}

export default Header