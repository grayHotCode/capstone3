
import { useState, useEffect } from 'react';
import Home from './pages/Home';
import AdminPanel from './pages/AdminPanel';
import Registration from './pages/Registration';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import AppNavbar from './components/AppNavbar';
import {Container} from 'react-bootstrap';
import { Route, Routes } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {


 //State hook for the user state that's define here for a global scope
  //Initialized as an object with properties from the localStorage
  //This will be used to store the information and will be used for validating if a user logged in on the app or not
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  //Function for clearing localStorage
  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect( () => {
    let token = localStorage.getItem('token');
    fetch('http://localhost:4000/users/details', {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(typeof data._id !== "undefined"){
        setUser({
          id:data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])
   
 

 return (
    <UserProvider value={{user, setUser, unsetUser}}>
    	<Router>
        <AppNavbar />
    		<Container>
    			<Routes>
            <Route exact path="/" element={<Home />}/>
            <Route exact path="/adminpanel" element={<AdminPanel />}/>
            <Route exact path="/products" element={<Products/>} />
    				<Route exact path="/registration" element={<Registration/>} />
    				<Route exact path="/login" element={<Login/>} />
            <Route exact path="/logout" element={<Logout/>} />
    			</Routes>
    		</Container>
    	</Router>
    </UserProvider>
  );


}

export default App;
