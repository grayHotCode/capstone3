import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Registration() {

    const {user} = useContext(UserContext);
    const history = useNavigate();

	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');


// Function to simulate user registration
    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`http://localhost:4000/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch(`http://localhost:4000/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setEmail('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Jamir Fruit Store!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        history("/login");

                        // history.push('/login')--for v5 of react-router-dom

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    };

                })
            };

        })

    }

useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [email, password1, password2]);

return (

 (user.id !== null) ?
            <Navigate to="/products" />
        :   

<Card className="text-center m-5">
	<Card.Title className="m-3"> Register</Card.Title>
	<Form onSubmit={(e) => registerUser(e)} className="m-3">
		<Form.Group className="m-3" controlId="email">
			<Form.Label>Email:</Form.Label>	
			<Form.Control className="text-center"
    	                type="email" 
    	                placeholder="Enter email"
                        value={email} 
                        onChange={e => setEmail(e.target.value)}
    	                required
                    />
		</Form.Group>

		<Form.Group className="m-3" controlId="password1">
			<Form.Label>Password:</Form.Label>
			<Form.Control className="text-center" 
    	                type="password" 
    	                placeholder="Password"
                        value={password1} 
                        onChange={e => setPassword1(e.target.value)}
    	                required
                    />	
		</Form.Group>

		<Form.Group className="m-3" controlId="password2">
			<Form.Label>Verify Password:</Form.Label>	
			<Form.Control className="text-center"
    	                type="password" 
    	                placeholder="Verify Password"
                        value={password2} 
                        onChange={e => setPassword2(e.target.value)}
    	                required
                    />
		</Form.Group>

		{ isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
        }

  		<Form.Group className="m-3">
  		<Form.Text>Already have an account?

  		<Card.Link href="http://localhost:3000/login"> Click here </Card.Link>  to log in</Form.Text>
  		</Form.Group>
	</Form>




















</Card>

	)













}