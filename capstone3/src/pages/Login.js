import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

  //Allow us to consume the User Context object and it's properties to use for user validation
    const { user, setUser} = useContext(UserContext);
  // State hooks to store the values of the input fields
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(true);


  function authenticate(e) {

    // Prevents page redirection via form submission
    e.preventDefault();

    fetch('http://localhost:4000/users/login',{
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password
        })

    })
    .then(res => res.json())
    .then(data => {
        console.log(data)

        if(typeof data.access !== "undefined"){
            localStorage.setItem('token', data.access)
            retrieveUserDetails(data.access)

            Swal.fire({
                title: 'Login Successful!',
                icon: 'success',
                text: 'Welcome to Jamir Fruit Store'
            })
            
        } else {

            Swal.fire({
                title: 'Authentication Failed.',
                icon: 'error',
                text: 'Check your login details'
            })
        }
    })

    setEmail('');
    setPassword('');


    // alert(`You are now logged in.`)
  
  }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers:{
                  Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


  useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

  }, [email, password]);




return (

  (user.id !== null) ? 
        <Navigate to="/home" />
        :

<Card className="text-center m-5">
	<Card.Title className="m-3"> Log In</Card.Title>
	<Form onSubmit={(e) => authenticate(e)} className="m-3">
		<Form.Group className="m-3" controlId="firstName">
			<Form.Label>Email:</Form.Label>	
			<Form.Control className="text-center"
    	                type="email" 
    	                placeholder="Enter email"
                        value={email} 
                        onChange={e => setEmail(e.target.value)}
    	                required
                    />
		</Form.Group>

		<Form.Group className="m-3" controlId="password">
			<Form.Label>Password:</Form.Label>
			<Form.Control className="text-center" 
    	                type="password" 
    	                placeholder="Enter your password"
                        value={password} 
                        onChange={e => setPassword(e.target.value)}
    	                required
                    />	
		</Form.Group>

		<Button variant="primary" type="submit">
    		Submit
  		</Button>

  		<Form.Group className="m-3">
  		<Form.Text>Don't have an account yet?

  		<Card.Link href="http://localhost:3000/Registration"> Click here </Card.Link>  to register</Form.Text>
  		</Form.Group>
	</Form>

</Card>

	)













}