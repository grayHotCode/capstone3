import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
// import ProductContext from '../ProductContext';

export default function AdminPanel() {

    // const {product} = useContext(ProductContext);
    const history = useNavigate();

	const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [isActive, setIsActive] = useState('');


// Function to simulate product registration
    function addProduct(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`http://localhost:4000/products/`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate product found',
                    icon: 'error',
                    text: 'Please provide a different product.'   
                });

            } else {

                fetch(`http://localhost:4000/products/`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setName('');
                        setPrice('');

                        Swal.fire({
                            title: 'Adding product successful',
                            icon: 'success',
                            text: 'Welcome to Jamir Fruit Store!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        // history("/login");

                        // history.push('/login')--for v5 of react-router-dom

                    } else {

                        Swal.fire({
                            title: 'Something wrong',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                    }

                })
            };

        })

    }

useEffect(() => {

        // Validation to enable submit button when all fields are populated 
        if(name !== '' && description !== '' && price !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description ,price]);

return (

 // (product.id !== null) ?
 //            <Navigate to="/adminpanel" />
 //        :   

<Card className="text-center m-5">
	<Card.Title className="m-3"> Dashboard</Card.Title>
	<Form onSubmit={(e) => addProduct(e)} className="m-3">
		<Form.Group className="m-3" controlId="name">
			<Form.Label>Name:</Form.Label>	
			<Form.Control className="text-center"
    	                type="text" 
                        value={name} 
                        onChange={e => setName(e.target.value)}
    	                required
                    />
		</Form.Group>

        <Form.Group className="m-3" controlId="description">
            <Form.Label>Description:</Form.Label>  
            <Form.Control className="text-center"
                        type="text" 
                        value={description} 
                        onChange={e => setDescription(e.target.value)}
                        required
                    />
        </Form.Group>

		<Form.Group className="m-3" controlId="price">
			<Form.Label>Price:</Form.Label>
			<Form.Control className="text-center" 
    	                type="text" 
                        value={price} 
                        onChange={e => setPrice(e.target.value)}
    	                required
                    />	
		</Form.Group>


		{ isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
        }

  		
	</Form>




















</Card>

	)













}