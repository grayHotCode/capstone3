const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require('../models/Product')



// checking if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;

		} else {

			return false;

		};
	});

};

// user registration

module.exports.registerUser = (reqBody) => {

			let newUser = new User({
				email : reqBody.email,
				password : bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) => {

				if (error) {

					return false;

				} else {

					return true;

		};

	});

};



// login route

module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {

			return ("ACCOUNT DOES NOT EXIST, PLEASE REGISTER");
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			}
			else {
				return ("PASSWORD IS INCORRECT")
			}
		}
	})
}



// get details

module.exports.getProfile = (data) => {
console.log(data)
			return User.findById(data.userId).then(result => {

				
				result.password = "";

				
				return result;

			});

		};

// order

module.exports.order = async (data) => {

	if(user.isAdmin !== true){

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.cart.push({productId: data.productId})

		return user.save().then((user, error) => {
			if(error){
				return false
			} else {
				return user
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(course => {

		product.orders.push({userId: data.userId})

		return product.save().then((product, error) => {
			if(error){
				return false
			} else {
				return product
			}
		})
	})

	if(isUserUpdated && isProductUpdated){
		return true
	} else {
		return false
	}

}else{

	return false
}

};
