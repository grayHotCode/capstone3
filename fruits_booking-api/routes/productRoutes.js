const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require('../auth')

// creating a product
router.post("/", auth.verify,  (req, res) => {

	// const userData = auth.decode(req.headers.authorization)

	// if(userData.isAdmin === true){
	// 	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	// }else{
	// 	res.send("Not Authorized");
	// }


		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));

	
});


// retrieving all products
router.get('/all',  (req, res) => {

	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// retrieve all active product(s)
router.get('/', (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// retrieve a specific product

router.get('/:productId', (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// update product

router.put('/:productId', auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// archiving product

router.put("/:product/archive", auth.verify, (req,res) => {
			let isAdmin = auth.decode(req.headers.authorization).isAdmin
			console.log(isAdmin)
			if (isAdmin) {
				courseController.archiveProduct(req.params, req.body).then(resultFromController => (res.send(`${resultFromController}. User is admin, course successfully archived!`)))
				
			}
			else {
				res.send(`User is not admin, cannot archive product.`)
			}
		})























module.exports = router;

