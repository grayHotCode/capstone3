const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const port = process.env.PORT || 4000

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors())

mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.wyjct.mongodb.net/fruits-booking?retryWrites=true&w=majority', 
	{
		
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection

	db.on('error', () => console.error.bind(console, 'Connection Error'))
	db.once('open', () => console.log('Now connected to MongoDB Atlas.'))


app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(port, () => console.log(`Server running on port ${port}`));

// https://polar-ravine-90941.herokuapp.com/ 